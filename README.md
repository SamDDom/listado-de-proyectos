# Índice de Proyectos de desarrollo de aplicaciones web  #
## IES Las Galletas | Curso 2020-21 ##

En este repositorio de listan los trabajos de proyecto llevados a cabo por el alumnado de 2º curso del Ciclo Formativo de Grado Superior "Desarrollo de aplicacines web" del IES Las Galletas durante el curso 2020-21.

### Indicaciones para el alumnado ###

Para cada proyecto, el alumnado deberá crear un repositorio público en GitHub o Bitbucket donde se incluirá:

* Archivo README.md
* /doc: Documento del proyecto en formato PDF y cualquier otra documentación generada (p. ej.: presentación, vídeos, etc.)
* /src: Código desarrollado

### Contenido del archivo README.md (repositorio del alumnado) ###

* Nombre del proyecto
* Apellidos y nombre de cada uno de los componentes del grupo
* Curso y ciclo formativo
* Nombre del centro educativo
* Curso escolar
* Breve descripción del proyecto
* Si lo hubiera, enlace a la aplicación en funcionamiento.

### Inclusión de los repositorios en este listado ###

Para la inclusión de los proyectos en este listado, el alumnado deberá hacer un "fork" de este repositorio (deberás tener una cuenta registrada en Bitbucket), editar el archivo README.md incluyendo un enlace al repositorio de su proyecto y realizar un "pull request" para que el profesor, tras su revisión, lo acepte.

### Edición del archivo README.md (repositorio de listado de proyectos) ###

El alumnado únicamente deberá editar el apartado "Listado de proyectos", y únicamente el punto correspondiente a su proyecto. Pudiendo editar el nombre del proyecto, así como añadir el enlace a su repositorio.

### Listado de proyectos ###

* Play Sounds | González González, Carmen Yi - Abad de Vera, Cristian Alexis
* Food Studies | Torres, Javier Ignacio - Rodríguez Mata, Luis David
* Nice & Tidy | Jiménez Rejada, Lorena - Faría Chávez, Víctor Manuel
* Infinity Gates | Beltrán Pérez, Andrew Nauzet - Verdera Infante, Juan José
* SimpleMarks | Arcila Laguna, Sergio Andrés - Puig Verastegui, Diana Carolina
* [Multifunctional Calendar](https://bitbucket.org/SamDDom/multifunctional_calendar/src/main/) | Diz Domínguez, Samuel - González Linares, Roberto - Valentina Medina, Andrea